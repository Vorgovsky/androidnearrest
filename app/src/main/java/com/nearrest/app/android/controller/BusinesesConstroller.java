package com.nearrest.app.android.controller;

import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nearrest.app.android.Constant;
import com.nearrest.app.android.listener.MapMarkersPrepareListener;
import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Coordinates;
import com.yelp.fusion.client.models.SearchResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

/**
 * Created by Админ on 10.02.2018.
 */

public class BusinesesConstroller {
    private double latitude;
    private double longitude;
    private ArrayList<Business> businesses;
    private ArrayList<MarkerOptions> markers;
    private GoogleMap googleMap;
    public BusinesesConstroller(){
        this.latitude = .0;
        this.longitude = .0;
    }
    public BusinesesConstroller(String latitude,String longitude){
        try {
            this.latitude =  Double.parseDouble( latitude.replace(",",".") );
            this.longitude =  Double.parseDouble( longitude.replace(",",".") );
            this.normalizeCoords();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    public BusinesesConstroller(double latitude,double longitude){
        this.latitude =  latitude;
        this.longitude =  longitude;
        this.normalizeCoords();
    }
    public double getLatitude(){
        return this.latitude;
    }
    public double getLongitude(){
        return this.latitude;
    }

    private void normalizeCoords(){
        if(latitude>= 90 || latitude<= -90 )
            this.latitude = this.latitude % 90;
        if(latitude>= 180 || latitude<= -180 )
            this.longitude = this.longitude % 180;
    }

    public ArrayList<MarkerOptions> getMarkers() {
        return markers;
    }

    public void setMarkers(ArrayList<MarkerOptions> markers) {
        this.markers = markers;
    }

    public ArrayList<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(ArrayList<Business> businesses) {
        this.businesses = businesses;
    }
}
