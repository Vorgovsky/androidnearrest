package com.nearrest.app.android.activity;

import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nearrest.app.android.Constant;
import com.nearrest.app.android.R;
import com.nearrest.app.android.controller.BusinesesConstroller;
import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.SearchResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

public class RestarauntMapActivity extends FragmentActivity implements OnMapReadyCallback {


    private BusinesesConstroller businesesConstroller;
    private ArrayList<MarkerOptions> markers;

    private GoogleMap mMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaraunt_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        businesesConstroller= new BusinesesConstroller(37.749626, -122.467392);
        PrepareMapTask prepareTask = new PrepareMapTask();
        prepareTask.execute();
    }
    class PrepareMapTask extends AsyncTask<String, Void, ArrayList<Business>> {

        private Exception exception;

        protected ArrayList<Business> doInBackground(String... param) {
            ArrayList<Business> bizs;
            try {
                YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();
                YelpFusionApi yelpFusionApi = apiFactory.createAPI(Constant.YELP_CLIENT_ID,
                        Constant.YELP_API_KEY);

                Map<String, String> params = new HashMap<>();

                params.put("term", "restaurants");
                params.put("latitude",String.valueOf(businesesConstroller.getLatitude()));
                params.put("longitude",String.valueOf(businesesConstroller.getLongitude()));

                Call<SearchResponse> call = yelpFusionApi.getBusinessSearch(params);

                SearchResponse response = call.execute().body();
                bizs = response.getBusinesses();
            } catch (Exception e) {
                this.exception = e;
                e.printStackTrace();
                return null;
            }
            return bizs;
        }

        protected void onPostExecute(ArrayList<Business> bizs) {
            ArrayList<MarkerOptions> marks = new ArrayList<>();
            if(!bizs.isEmpty())
                for(Business biz:bizs){
                    marks.add(new MarkerOptions()
                            .position(new LatLng(biz.getCoordinates().getLatitude(),
                                    biz.getCoordinates().getLongitude()))
                            .title(biz.getName()));
                }
            markers = marks;
            for(MarkerOptions mark:markers){
                mMap.addMarker(mark);
            }
            businesesConstroller.setBusinesses(bizs);
        }
    }

}
